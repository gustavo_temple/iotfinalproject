'use strict';

const functions = require('firebase-functions');

// Include a Service Account Key to use a Signed URL
const gcs = require('@google-cloud/storage')({keyFilename: 'service-account-credentials.json'});
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const path = require('path');
var http = require('http');
const os = require('os');
const fs = require('fs');

/**
 * When an image is uploaded in the Storage bucket We send a GCM to the added.
 */
exports.sendFirebaseCloudMessage = functions.storage.object().onChange(event => {
  
  // File and directory paths.
  const filePath = event.data.name;
  const fileDir = path.dirname(filePath);
  const fileName = path.basename(filePath);

  const fullImagePath = path.join(fileDir, fileName);

  // Cloud Storage files.
  const bucket = gcs.bucket(event.data.bucket);
  const file = bucket.file(filePath);

  var post_data = 
    { "data": 
        {
            "file_path": filePath,
	    "file_path_2": fullImagePath,
            "message": "New picture uploaded to storage"
        },
      "to" : "eDWJbqRAgA4:APA91bHXpCHi85ac9_iYtaiQ33tLSjN4hZ1C9w50DdX_Zw1AGCuZf6syb17XGfGOzRRdFY5TlfJzt_kdxVn4In7mxZf6AxnbDzBTzkNeRzjV8q1anaupOJZ2jotBfx2GoRglYki52Tpc"
    }

  // An object of options to indicate where to post to
  var post_options = {
      host: 'fcm.googleapis.com',
      port: '443',
      path: '/fcm/send',
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': 'key=AIzaSyDvg6NExRRtUV_LzhTCfp4-qD-va9rG8WQ'
      }
  };

  // Set up the request
  var post_req = http.request(post_options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
          console.log('Response: ' + chunk);
      });
  });

  // post the data
  post_req.write(post_data);
  post_req.end();

});
