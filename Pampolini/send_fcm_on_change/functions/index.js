'use strict';

const functions = require('firebase-functions');

// Include a Service Account Key to use a Signed URL
const gcs = require('@google-cloud/storage')({keyFilename: 'service-account-credentials.json'});
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const path = require('path');
var http = require('http');
var request = require('request');
const os = require('os');
const fs = require('fs');
const firebase = require('firebase');

/**
 * When an image is uploaded in the Storage bucket We send a GCM to the added.
 */
exports.sendFirebaseCloudMessage = functions.storage.object().onChange(event => {
  
  // File and directory paths.
  const filePath = event.data.name;
  const fileDir = path.dirname(filePath);
  const fileName = path.basename(filePath);

  const fullImagePath = path.join(fileDir, fileName);

  // Cloud Storage files.
  const bucket = gcs.bucket(event.data.bucket);
  const file = bucket.file(filePath);

  // Initialize connection with app, if it has not being initialized yet
  if (firebase.apps.length === 0) {
    // Database config
    var config = {
      apiKey: "AIzaSyCBCjiUydFRki-47TP8pqjMa0awo-VxcCg",
      authDomain: "smartdoorbell-iot-inf747.firebaseapp.com",
      databaseURL: "https://smartdoorbell-iot-inf747.firebaseio.com",
      storageBucket: "smartdoorbell-iot-inf747.appspot.com"
    };
    firebase.initializeApp(config);
  }

  // Get a reference to the database service
  var database = firebase.database();

  // Get current database state
  database.ref('token_id').once('value').then(function(snapshot){

    var toke_id = snapshot.val();

    var post_data = 
      { "data": 
          {
              "file_path": filePath,
              "message": "New doorbell notification"
          },
        "to" : toke_id
      }

    request({
        url: "https://fcm.googleapis.com/fcm/send",
        method: "POST",
        json: true,
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'key=AIzaSyA1oau_ivnpX5jmQ5-aPCCPk-dyAiVN6Oo'
        },
        body: post_data
    }, function (error, response, body){
        console.log(response);
    });

  });

});
