# Send Firebase Message to app when storage change

## Functions Code

File [functions/index.js](functions/index.js) has the implementation itself in Node.js.

This functions listens to events on the Firebase storage, and whenever there is a new picture included into the storage, a new cloud message is sento to the device.
We keep track of the lates device token id using a reference ("token_id") on the Firebase real time database.

## Trigger rules

The function triggers on upload of any file to your Firebase project's default Cloud Storage bucket.

## Deploy and test

To deploy and test the sample:

 - Create a Firebase project on the [Firebase Console](https://console.firebase.google.com) and visit the **Storage** tab.
 - Include this folder with the source code
 - Open this sample's directory: `cd send_fcm_on_change`
 - There should be a reference called "token_id" inside yout Firebase real time database. This reference should be updated by your app first
 - Include the necessary keys inside the script
 - Setup your project by running `firebase use --add` and select the project you had created.
 - Install dependencies in the functions directory: `cd functions; npm install; cd -`
 - Go to the Firebase Console, select the gear image > **project settings** > **Service Accounts** and click **Generate New Private Key** to download a Service Account Key JSON document.
 - Add the JSON service account credentials file to the **Functions** directory as **functions/service-account-credentials.json**.
 - Deploy your project using `firebase deploy`
 - Go to the Firebase Console **Storage** tab and upload an image. After a short time an thumbnail image with the same name but a `thumb_` prefix will be created in the same folder (make sure you refresh the UI to see the new file).
 - Go to the Firebase Console **Database** tab and see that a child of `images` has been created contiaining the Signed URLs for both images.
