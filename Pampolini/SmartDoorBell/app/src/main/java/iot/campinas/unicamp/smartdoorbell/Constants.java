package iot.campinas.unicamp.smartdoorbell;

/**
 * Created by lgpampol on 18/10/17.
 */

public class Constants {
    public static final String MY_SHARED_PREFERENCES = "DoorBellSharedPreferences";
    public static final String DOOR_STATE = "doorState";
}
