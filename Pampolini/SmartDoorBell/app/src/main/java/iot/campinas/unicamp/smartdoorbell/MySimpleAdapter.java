package iot.campinas.unicamp.smartdoorbell;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by lgpampol on 24/10/17.
 */

public class MySimpleAdapter extends ArrayAdapter<String> {

    ArrayList<String> imgName;
    ArrayList<String> imgPath;

    Context mContext;

    public MySimpleAdapter(Context context, ArrayList<String> itemname, ArrayList<String> imgid) {
        super(context, R.layout.list_item, itemname);

        this.imgName = itemname;
        this.imgPath = imgid;
        mContext = context;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View rowView = inflater.inflate(R.layout.list_item, null, true);
        Bitmap myBitmap = null;
        ViewHolder targetView = (ViewHolder) rowView.getTag();

        if (targetView == null) {
            targetView = new ViewHolder();
            targetView.img = rowView.findViewById(R.id.my_image);
            targetView.txt = rowView.findViewById(R.id.my_text);

            myBitmap = BitmapFactory.decodeFile(this.imgPath.get(position));
            targetView.img.setImageBitmap(myBitmap);
            targetView.txt.setText(this.imgName.get(position));
            rowView.setTag(targetView);
        }

        return rowView;
    }


    static class ViewHolder {
        ImageView img;
        TextView txt;
    }


}
