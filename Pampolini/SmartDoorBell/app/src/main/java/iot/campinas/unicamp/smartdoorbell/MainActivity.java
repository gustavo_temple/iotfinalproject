package iot.campinas.unicamp.smartdoorbell;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ToggleButton;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getName();
    private FirebaseAuth mAuth;
    private ProgressDialog mProgressDialog;
    static final int WEAPON_ALERT = 3;

    ArrayList<String> imgPath = new ArrayList<>();
    ArrayList<String> imgName = new ArrayList<>();
    MySimpleAdapter mAdapter;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(FirebaseDownloadService.DOWNLOAD_COMPLETED)) {
                String filePath = intent.getStringExtra(FirebaseDownloadService.EXTRA_DOWNLOAD_PATH);
                imgPath.add(0, filePath);
                imgName.add(0, DateFormat.getDateTimeInstance().format(new Date()));
                mAdapter.notifyDataSetChanged();
            }
        }
    };
    LocalBroadcastManager broadcastManager;

    private void showProgressDialog(String caption) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.setMessage(caption);
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void signInAnonymously() {
        // Sign in anonymously. Authentication is required to read or write from Firebase Storage.
        showProgressDialog(getString(R.string.progress_auth));
        mAuth.signInAnonymously()
                .addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        Log.d(TAG, "signInAnonymously:SUCCESS");
                        hideProgressDialog();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Log.e(TAG, "signInAnonymously:FAILURE", exception);
                        hideProgressDialog();
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        broadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(FirebaseDownloadService.DOWNLOAD_COMPLETED);
        broadcastManager.registerReceiver(broadcastReceiver, intentFilter);

        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Token: " + token);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseRef = database.getReference("token_id");
        databaseRef.setValue(token);

        // Include listener for door status
        databaseRef = database.getReference("door_opened");
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Update shared preferences
                Boolean value = dataSnapshot.getValue(Boolean.class);
                if(value != null) {
                    ToggleButton doorStateToggle = findViewById(R.id.change_door_state);
                    doorStateToggle.setChecked(value);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        // Include listener for door status
        databaseRef = database.getReference("weapon_alarm");
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Update shared preferences
                Boolean value = dataSnapshot.getValue(Boolean.class);
                if(value) {

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                    PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* requestCode */, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);

                    NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                            .setSmallIcon(R.drawable.ic_weapon_alert)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(getString(R.string.weapon_alert))
                            .setAutoCancel(true)
                            .setVibrate(new long[0])
                            .setPriority(Notification.PRIORITY_MAX)
                            .setContentIntent(pendingIntent);

                    NotificationManager manager =
                            (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    manager.notify(WEAPON_ALERT, builder.build());
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        signInAnonymously();

        // Update Toogle state
        ToggleButton doorStateToggle = findViewById(R.id.change_door_state);
        doorStateToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference databaseRef = database.getReference("door_opened");
                if (isChecked) {
                    databaseRef.setValue(true);
                } else {
                    databaseRef.setValue(false);
                }
            }
        });

        File directory = getApplicationContext().getFilesDir();
        String targetPath = directory.getPath();

        File targetDirector = new File(targetPath);
        File[] files = targetDirector.listFiles();

        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

        for (File file : files){
            imgName.add(0, currentDateTimeString);
            imgPath.add(0, file.getAbsolutePath());
        }

        mAdapter = new MySimpleAdapter(this, imgName, imgPath);

        ListView list = findViewById(R.id.my_list);
        list.setAdapter(mAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        broadcastManager.unregisterReceiver(broadcastReceiver);
    }
}
