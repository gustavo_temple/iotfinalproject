"use strict";

const config = require('./config.js');

const fs = require('fs');
const mraa = require('mraa');
const util = require('util');
const exec = require('child_process').exec;
const mime = require('mime-types');
const base64url = require('base64-url');

// Load and configre GCS
const firebase_apifile = config.firebase.api_file;
const project_id = require(firebase_apifile).project_id;
const gcs = require('@google-cloud/storage')({projectId: project_id, keyFilename: firebase_apifile});
const bucket = gcs.bucket(util.format('%s.appspot.com', project_id));

// Load and configure Firebase
const firebase = require('firebase');
firebase.initializeApp({
    databaseURL: util.format('https://%s.firebaseio.com', project_id),
    serviceAccount: firebase_apifile
});

log('running... ');

// Setup doorbell button input and interrupt routine
let doorbell_pin = new mraa.Gpio(config.doorbell_pin);
doorbell_pin.isr(mraa.EDGE_FALLING, doorbell_ISR);

/**
 * Interrupt routine for doorbell input button
 * It will run ffmpeg to take a picture and upload it to the cloud services
 */
function doorbell_ISR(){
    log("Someone's at the door.");
    
    var picturepath = util.format("%s/%s",config.file_folder, config.picture_name);

    if(fs.existsSync(picturepath)) fs.unlinkSync(picturepath);

    exec(util.format("%s %s %s", config.ffmpeg_path, config.ffmpeg_cmd, picturepath), function(error){
    	if(error){
            log(error);
        }
        else {
            log("Picture taken. Uploading...");
            upload_image(picturepath);
        }
    });
}

/**
 * Function will upload an image pointed by the given path to firebase and cloud storage
 */
function upload_image(path){
    var file_config  = {
        destination: util.format('/pampolini-door/%s.jpg', getTimestamp()),
        public: false,
        metadata: {
            contetType: mime.lookup(path)
        }
    };
    
    // gcs bucket upload
    bucket.upload(path, file_config, function(error, file){
        if(error){
            log(error);
        }
        else {
            log('Picture sent for analisys');
        }
    });

    // firebase database upload
    firebase.database().ref('/pampolini-door').push({
        faces: -1,
        weapons: -1,
        timestamp: new Date().getTime(),
        image: base64_encode(path)
    });
}

function log(message){
    console.log('[%s] Smart doorbell: %s', new Date(), message);
}

function base64_encode(path){
    return base64url.escape(new Buffer(fs.readFileSync(path)).toString('base64'));
}

function getTimestamp(){
    return new Date().toISOString().replace(/-|T|:|\..+/g, ''); 
}

// Set interval loop to keep the service running
setInterval(function(){}, 10000);
