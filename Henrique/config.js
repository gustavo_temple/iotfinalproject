module.exports = (function(){
    return {
        doorbell_pin: 2,
	file_folder: 'frames',
	picture_name: 'frame1.jpg',
	ffmpeg_path: '/home/root/bin/ffmpeg/ffmpeg',
	ffmpeg_cmd: '-f video4linux2 -i /dev/video0 -vframes 1',
	firebase: {
	    api_file: './smart-doorbell-apikey.json'
	}
    };
})();
