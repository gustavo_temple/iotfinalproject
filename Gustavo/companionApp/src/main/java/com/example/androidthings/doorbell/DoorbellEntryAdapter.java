package com.example.androidthings.doorbell;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;


/**
 * RecyclerView adapter to populate doorbell entries from Firebase.
 */
class DoorbellEntryAdapter extends FirebaseRecyclerAdapter<DoorbellEntry, DoorbellEntryAdapter.DoorbellEntryViewHolder> {

    private static final String TAG = DoorbellEntryAdapter.class.getSimpleName();

    /**
     * ViewHolder for each doorbell entry
     */
    public static class DoorbellEntryViewHolder extends RecyclerView.ViewHolder {

        public final ImageView image;
        public final TextView time;
        public final TextView faces;
        public final TextView weapons;

        public DoorbellEntryViewHolder(View itemView) {
            super(itemView);

            this.image = itemView.findViewById(R.id.imageView1);
            this.time = itemView.findViewById(R.id.textView1);
            this.faces = itemView.findViewById(R.id.textView2);
            this.weapons = itemView.findViewById(R.id.textView3);
        }
    }

    private Context mApplicationContext;

    DoorbellEntryAdapter(Context context, DatabaseReference ref) {
        super(DoorbellEntry.class, R.layout.doorbell_entry, DoorbellEntryViewHolder.class, ref);

        mApplicationContext = context.getApplicationContext();
    }

    @Override
    protected void populateViewHolder(DoorbellEntryViewHolder viewHolder,
                                      DoorbellEntry model,
                                      int position) {
        try {
            // Display the timestamp
            if (model.getTimestamp() != null) {
                CharSequence prettyTime = DateUtils.getRelativeDateTimeString(mApplicationContext,
                        model.getTimestamp(), DateUtils.SECOND_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, 0);
                viewHolder.time.setText(prettyTime);
            }

            // Display the image
            if (model.getImage() != null) {
                // Decode image data encoded by the Cloud Vision library
                byte[] imageBytes = Base64.decode(model.getImage(), Base64.NO_WRAP | Base64.URL_SAFE);
                Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                if (bitmap != null) {
                    viewHolder.image.setImageBitmap(bitmap);
                } else {
                    Drawable placeholder =
                            ContextCompat.getDrawable(mApplicationContext, R.drawable.ic_image);
                    viewHolder.image.setImageDrawable(placeholder);
                }
            }

            // Display the faces
            if (model.getFaces() != null) {
                viewHolder.faces.setText(TextUtils.concat("Faces: " + model.getFaces()));
            } else {
                viewHolder.faces.setText("no annotations yet");
            }

            // Display the weapons
            if (model.getWeapons() != null) {
                viewHolder.weapons.setText(TextUtils.concat("Weapons: " + model.getWeapons()));
            } else {
                viewHolder.weapons.setText("no annotations yet");
            }
        } catch (OutOfMemoryError e) {
            Log.e(TAG, e.toString());
        }
    }
}
