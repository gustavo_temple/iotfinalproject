package com.example.androidthings.doorbell;

/**
 * Model class for Firebase data entries
 */
public class DoorbellEntry {

    Long timestamp;
    String image;
    Integer faces;
    Integer weapons;

    public DoorbellEntry() {
    }

    public DoorbellEntry(Long timestamp, String image, Integer faces, Integer weapons) {
        this.timestamp = timestamp;
        this.image = image;
        this.faces = faces;
        this.weapons = weapons;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public String getImage() {
        return image;
    }

    public Integer getFaces() {
        return faces;
    }

    public Integer getWeapons() {
        return weapons;
    }
}