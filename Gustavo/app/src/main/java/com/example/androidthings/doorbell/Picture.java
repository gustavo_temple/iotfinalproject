package com.example.androidthings.doorbell;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Picture implements Serializable {

    public long timestamp;
    public String image;
    public int faces;
    public int weapons;

    public static final String IMAGE = "image";
    public static final String TIMESTAMP = "timestamp";
    public static final String FACES = "faces";
    public static final String WEAPONS = "weapons";

    public Picture() {
        // Default constructor required for calls to DataSnapshot.getValue(Picture.class)
    }

    public Picture(long timestamp,
                   String image,
                   int faces,
                   int weapons) {
        this.timestamp = timestamp;
        this.image = image;
        this.faces = faces;
        this.weapons = weapons;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(IMAGE, image);
        result.put(TIMESTAMP, timestamp);
        result.put(FACES, faces);
        result.put(WEAPONS, weapons);
        return result;
    }
}