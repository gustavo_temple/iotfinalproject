package com.example.androidthings.doorbell;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;

import com.google.android.things.contrib.driver.button.Button;
import com.google.android.things.contrib.driver.button.ButtonInputDriver;
import com.google.android.things.contrib.driver.pwmspeaker.Speaker;
import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManagerService;
import com.google.api.services.vision.v1.model.AnnotateImageResponse;
import com.google.api.services.vision.v1.model.Feature;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class DoorbellActivity extends Activity {
    private static final String TAG = DoorbellActivity.class.getSimpleName();
    private static final String PICTURES = "pampolini-door";
    private static final String DOOR_OPENED = "door_opened";
    private static final String WEAPON_ALARM = "weapon_alarm";
    private static final String FACE_DETECTION = "FACE_DETECTION";
    private static final String LABEL_DETECTION = "LABEL_DETECTION";
    private static final int FREQ = 7000;

    private volatile Speaker mSpeaker;
    private AtomicBoolean ledState = new AtomicBoolean(false);
    private Gpio mLed;

    private DatabaseReference doorRef;
    private DatabaseReference alarmRef;

    /**
     * A {@link Handler} for running Cloud tasks in the background.
     */
    private Handler mFacesCloudHandler;
    private Handler mWeaponsCloudHandler;

    /**
     * An additional thread for running Cloud tasks that shouldn't block the UI.
     */
    private HandlerThread mFacesCloudThread;
    private HandlerThread mWeaponsCloudThread;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "Doorbell Activity created.");

        final PeripheralManagerService pioService = new PeripheralManagerService();

        try {
            mLed = pioService.openGpio(BoardDefaults.getGPIOForLED());
            mLed.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
            setLedValue();
            ButtonInputDriver mButtonInputDriver = new ButtonInputDriver(
                    BoardDefaults.getGPIOForButton(),
                    Button.LogicState.PRESSED_WHEN_LOW,
                    KeyEvent.KEYCODE_SPACE);
            mButtonInputDriver.register();
            mSpeaker = new Speaker(BoardDefaults.getPwmPin());
            mSpeaker.stop();

            final DatabaseReference picturesRef = FirebaseDatabase.getInstance().getReference().child(PICTURES);
            picturesRef.addChildEventListener(picListener);

            doorRef = FirebaseDatabase.getInstance().getReference().child(DOOR_OPENED);
            doorRef.addListenerForSingleValueEvent(initializeDoor);
            doorRef.addValueEventListener(doorListener);

            alarmRef = FirebaseDatabase.getInstance().getReference().child(WEAPON_ALARM);
            alarmRef.addListenerForSingleValueEvent(initializeAlarm);
            alarmRef.addValueEventListener(alarmListener);

            mFacesCloudThread = new HandlerThread("FacesCloudThread");
            mFacesCloudThread.start();
            mFacesCloudHandler = new Handler(mFacesCloudThread.getLooper());

            mWeaponsCloudThread = new HandlerThread("WeaponsCloudThread");
            mWeaponsCloudThread.start();
            mWeaponsCloudHandler = new Handler(mWeaponsCloudThread.getLooper());
        } catch (IOException e) {
            Log.e(TAG, e.toString());
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.i(TAG, "onKeyUp: " + keyCode);

        if (keyCode == KeyEvent.KEYCODE_SPACE) {

            alarmRef.setValue(false);
            ledState.set(!ledState.get());
            doorRef.setValue(ledState.get());
            return true;
        }

        return super.onKeyUp(keyCode, event);
    }

    private final ChildEventListener picListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            Log.i(TAG, "onChildAdded DataSnapshot: " + dataSnapshot);

            Picture pic = dataSnapshot.getValue(Picture.class);

            if (pic.weapons == -1) {
                detectWeapons(dataSnapshot.getRef(), pic.image);
            }

            if (pic.faces == -1) {
                detectFaces(dataSnapshot.getRef(), pic.image);
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            Log.i(TAG, "onChildChanged DataSnapshot: " + dataSnapshot);
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            Log.i(TAG, "onChildRemoved DataSnapshot: " + dataSnapshot);
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            Log.i(TAG, "onChildMoved DataSnapshot: " + dataSnapshot);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(TAG, "onCancelled: " + databaseError);
        }
    };

    private final ValueEventListener doorListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.i(TAG, "doorListener");
            Boolean value = (Boolean) dataSnapshot.getValue();
            ledState.set(value);
            setLedValue();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(TAG, "onCancelled: " + databaseError);
        }
    };

    private final ValueEventListener initializeDoor = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.i(TAG, "initializeDoor");
            Boolean value = (Boolean) dataSnapshot.getValue();
            ledState.set(value);
            setLedValue();
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(TAG, "onCancelled: " + databaseError);
        }
    };

    private final ValueEventListener alarmListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.i(TAG, "alarmListener");
            Boolean value = (Boolean) dataSnapshot.getValue();
            try {
                if (value) {
                    mSpeaker.play(FREQ);
                } else {
                    mSpeaker.stop();
                }
            } catch (IOException e) {
                Log.e(TAG, e.toString());
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(TAG, "onCancelled: " + databaseError);
        }
    };

    private final ValueEventListener initializeAlarm = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.i(TAG, "initializeAlarm");
            Boolean value = (Boolean) dataSnapshot.getValue();
            try {
                if (value) {
                    mSpeaker.play(FREQ);
                } else {
                    mSpeaker.stop();
                }
            } catch (IOException e) {
                Log.e(TAG, e.toString());
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e(TAG, "onCancelled: " + databaseError);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mLed != null) {
            try {
                mLed.close();
            } catch (IOException e) {
                Log.e(TAG, "Error closing LED GPIO", e);
            } finally {
                mLed = null;
            }
            mLed = null;
        }

        if (mSpeaker != null) {
            try {
                mSpeaker.stop();
                mSpeaker.close();
            } catch (IOException e) {
                Log.e(TAG, "Error closing speaker", e);
            } finally {
                mSpeaker = null;
            }
        }

        mFacesCloudThread.quitSafely();
        mWeaponsCloudThread.quitSafely();
    }

    /**
     * Update the value of the LED output.
     */
    private void setLedValue() {
        Log.i(TAG, "setLedValue: " + ledState.get());

        try {
            mLed.setValue(ledState.get());
        } catch (IOException e) {
            Log.e(TAG, "Error updating GPIO value", e);
        }
    }

    private void detectFaces(final DatabaseReference ref,
                             final String image) {
        Log.i(TAG, "detectFaces()");

        if (image == null || ref == null) {
            return;
        }

        mFacesCloudHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "sending image to cloud vision: faces");
                try {
                    final Feature faceDetection = new Feature();
                    faceDetection.setType(FACE_DETECTION);

                    byte[] bytes = Base64.decode(image, Base64.NO_WRAP | Base64.URL_SAFE);

                    List<AnnotateImageResponse> images = CloudVisionUtils.annotateImage(reduceImage(bytes),
                            faceDetection);

                    int faces = CloudVisionUtils.facesDetected(images);

                    Log.d(TAG, "Faces: " + faces);

                    if (faces == 1) {
                        doorRef.setValue(true);
                    } else {
                        doorRef.setValue(false);
                    }

                    ref.child(Picture.FACES).setValue(faces);
                } catch (IOException e) {
                    Log.e(TAG, "Cloud Vison API error: ", e);
                }
            }
        });
    }

    private void detectWeapons(final DatabaseReference ref,
                               final String image) {
        Log.i(TAG, "detectWeapons()");

        if (image == null || ref == null) {
            return;
        }

        mWeaponsCloudHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "sending image to cloud vision: weapons");
                try {
                    final Feature weaponDetection = new Feature();
                    weaponDetection.setType(LABEL_DETECTION);

                    byte[] bytes = Base64.decode(image, Base64.NO_WRAP | Base64.URL_SAFE);

                    List<AnnotateImageResponse> images = CloudVisionUtils.annotateImage(reduceImage(bytes),
                            weaponDetection);

                    boolean isWeaponDetected = CloudVisionUtils.isWeaponDetected(images);

                    Log.d(TAG, "isWeaponDetected: " + isWeaponDetected);

                    if (isWeaponDetected) {
                        doorRef.setValue(false);
                        alarmRef.setValue(true);
                        ref.child(Picture.WEAPONS).setValue(1);
                    } else {
                        ref.child(Picture.WEAPONS).setValue(0);
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Cloud Vison API error: ", e);
                }
            }
        });
    }

    /**
     * @param bytes byte []
     * @return reduceImage
     */
    private byte[] reduceImage(byte[] bytes) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        Log.i(TAG, "Bytes before: " + bitmap.getByteCount());
        Bitmap resized = Bitmap.createScaledBitmap(bitmap,
                (int) (bitmap.getWidth() * 0.8),
                (int) (bitmap.getHeight() * 0.8),
                true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resized.compress(Bitmap.CompressFormat.JPEG,
                80,
                stream);
        byte[] byteArray = stream.toByteArray();
        Log.i(TAG, "Bytes after: " + byteArray.length);
        return byteArray;
    }
}
