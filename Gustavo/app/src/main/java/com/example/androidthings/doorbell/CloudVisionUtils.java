package com.example.androidthings.doorbell;

import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.vision.v1.Vision;
import com.google.api.services.vision.v1.VisionRequestInitializer;
import com.google.api.services.vision.v1.model.AnnotateImageRequest;
import com.google.api.services.vision.v1.model.AnnotateImageResponse;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesRequest;
import com.google.api.services.vision.v1.model.BatchAnnotateImagesResponse;
import com.google.api.services.vision.v1.model.EntityAnnotation;
import com.google.api.services.vision.v1.model.Feature;
import com.google.api.services.vision.v1.model.Image;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

class CloudVisionUtils {
    private static final String CLOUD_VISION_API_KEY = "AIzaSyC0MN1pr48MnaqZ8yYxjPq9hRnOgpLvh4c";
    private static final String GUN = "gun";
    private static final String KNIFE = "knife";
    private static final String WEAPON = "weapon";
    private static final String TAG = CloudVisionUtils.class.getSimpleName();
    private static final int MAX_LABEL_RESULTS = 5;

    static List<AnnotateImageResponse> annotateImage(byte[] imageBytes,
                                                     final Feature feature) throws IOException {
        Log.d(TAG, "Start Cloud Vision");

        // Construct the Vision API instance
        HttpTransport httpTransport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = GsonFactory.getDefaultInstance();
        VisionRequestInitializer initializer = new VisionRequestInitializer(CLOUD_VISION_API_KEY);
        Vision vision = new Vision.Builder(httpTransport, jsonFactory, null)
                .setVisionRequestInitializer(initializer)
                .build();

        // Create the image request
        AnnotateImageRequest imageRequest = new AnnotateImageRequest();
        Image img = new Image();
        img.encodeContent(imageBytes);
        imageRequest.setImage(img);

        // Add the features we want
        feature.setMaxResults(MAX_LABEL_RESULTS);
        imageRequest.setFeatures(Collections.singletonList(feature));

        // Batch and execute the request
        BatchAnnotateImagesRequest requestBatch = new BatchAnnotateImagesRequest();
        requestBatch.setRequests(Collections.singletonList(imageRequest));
        BatchAnnotateImagesResponse response = vision.images()
                .annotate(requestBatch)
                // Due to a bug: requests to Vision API containing large images fail when GZipped.
                .setDisableGZipContent(true)
                .execute();

        Log.d(TAG, "Cloud Vision OK");

        return response.getResponses();
    }

    static boolean isWeaponDetected(List<AnnotateImageResponse> responses) {
        if (responses.get(0).isEmpty()) {
            return false;
        }

        List<EntityAnnotation> labels = responses.get(0).getLabelAnnotations();
        if (labels != null) {
            for (EntityAnnotation label : labels) {
                String desc = label.getDescription().toLowerCase();
                if (desc.contains(GUN)
                        || desc.contains(WEAPON)
                        || desc.contains(KNIFE)) {
                    return true;
                }
            }
        }

        return false;
    }

    static int facesDetected(List<AnnotateImageResponse> responses) {
        if (responses.get(0).isEmpty()) {
            return 0;
        }

        return responses.get(0).getFaceAnnotations().size();
    }
}
